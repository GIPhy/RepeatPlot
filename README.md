# RepeatPlot

_RepeatPlot_ is a command line program written in [Java](https://docs.oracle.com/en/java/) to create [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG) figures that represent the positions of each pair of (long) repeat regions in a prokaryote chromosome. _RepeatPlot_ implements a fast _k_-mer-based algorithm to find every pair of similar (repeat) regions. The graphical representation of the position of each pair of similar repeat regions is inspired by the pioneering work of Rocha et al. (1999).

## Installation

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/RepeatPlot.git
```

## Compilation and execution

The source code of _RepeatPlot_ is inside the _src_ directory. It requires **Java 11** (or higher) to be compiled.

#### Building an executable jar file

On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (**11** or higher) installed, a Java executable jar file can be created by typing the following command lines inside the _src_ directory:

```bash
javac RepeatPlot.java
echo Main-Class: RepeatPlot > MANIFEST.MF
jar -cmvf MANIFEST.MF RepeatPlot.jar RepeatPlot.class
rm MANIFEST.MF RepeatPlot.class
```

This will create the executable jar file `RepeatPlot.jar` that can be run with the following command line model:

```bash
java -jar RepeatPlot.jar [options]
```

#### Building a native code binary

On computers with [GraalVM](https://www.graalvm.org/downloads/) installed, a native executable can be built by typing the following command lines inside the _src_ directory:

```bash
javac RepeatPlot.java
native-image RepeatPlot RepeatPlot
rm RepeatPlot.class
```
This will create the native executable `RepeatPlot` that can be run with the following command line model:
```bash
./RepeatPlot [options]
```



## Usage

Run _RepeatPlot_ without option to read the following documentation:

```
USAGE: RepeatPlot -i <fasta> -o <svg> [-l <length>] [-c <color>] [-r <scale>] [-v]

OPTIONS:
  -i <file>    input FASTA file name; should end with .gz when gzipped (mandatory)
  -o <file>    output SVG file name (mandatory)
  -l <int>     minimum repeat region length (default: 50)
  -c <string>  circle color: black, blue,  brown, gray, green,  orange, red, sand,
               violet (default: red)
  -r <real>    radius scale factor (default: 1.0)
  -v           prints info into log file (default: not set)
```

## Notes

* First, _RepeatPlot_ determines the _x_ canonical _k_-mers (_k_ = 15) that occur at least twice in the input sequence. Second, _RepeatPlot_ selects the _r_ sequence regions _R<sub>i</sub>_ composed by a set _K<sub>i</sub>_ of successive non-unique canonical _k_-mers such that |&nbsp;_K<sub>i</sub>_&nbsp;|&nbsp;&geq;&nbsp;_l_&nbsp;&minus;&nbsp;_k_&nbsp;+&nbsp;1  (_l_&nbsp;=&nbsp;50 by default; option `-l`). Finally, _RepeatPlot_ compares the _k_-mer contents of each pair _R<sub>i</sub>_, _R<sub>j</sub>_ of selected regions. Two regions _R<sub>i</sub>_ and _R<sub>j</sub>_ are assessed as similar when |&nbsp;_K<sub>i</sub>_&nbsp;&cap;&nbsp;_K<sub>j</sub>_&nbsp;|&nbsp;&geq;&nbsp;_l_&nbsp;&minus;&nbsp;_k_&nbsp;+&nbsp;1 . Every pair of similar regions is plotted as a circle of radius proportional to |&nbsp;_K<sub>i</sub>_&nbsp;&cap;&nbsp;_K<sub>j</sub>_&nbsp;|. Given an input sequence of length _n_ nucleotide residues, the overall time complexity required by _RepeatPlot_ is _O_&nbsp;(&nbsp;_n_&nbsp;+&nbsp;_x_&nbsp;_r_<sup>2</sup>&nbsp;), therefore enabling fast running times in many cases.

* Denoting _F_<sub>1</sub>, _F_<sub>0</sub> and _f_<sub>1</sub>, the total number of _k_-mers, distinct _k_-mers, and unique (singleton) _k_-mers, respectively (e.g. Melsted and Halldórsson 2014), it is worth noting that _x_&nbsp;=&nbsp;_F_<sub>0</sub>&nbsp;&minus;&nbsp;_f_<sub>1</sub>. _RepeatPlot_ computes and displays the four values _F_<sub>1</sub>, _F_<sub>0</sub>, _f_<sub>1</sub> and _x_ (_k_ = 15), as well as the _k_-mer repeat index (&nbsp;_F_<sub>1</sub>&nbsp;&minus;&nbsp;_f_<sub>1</sub>&nbsp;)&nbsp;/&nbsp;_F_<sub>0</sub>. The _k_-mer repeat index is as close to 0 (e.g. &leq; 0.05) as the specified chromosome sequence is mainly made up by unique _k_-mers; reciprocally, quite large _k_-mer repeat index values (e.g. &geq; 0.15) are indicative of the presence of highly-occurring repeated _k_-mers.

* The input file should be in FASTA format (UTF-8 character encoding) and may contain at least one nucleotide sequence. If the input file contains more than one sequence, they are concatenated to obtain a unique one. When compressed using _gzip_, the input file name should end with `.gz`.

* The _RepeatPlot_ figure is written into the specified output file in [SVG](https://developer.mozilla.org/en-US/docs/Web/SVG) format. Circle diameters can be increased or decreased using option `-r` (>&nbsp;1 or <&nbsp;1, respectively). Circle colors can be modified using option `-c` (default: red).


## Examples

The directory _example/_ contains several SVG files created using _RepeatPlot_.

The chromosome sequences of nine different bacterial strains can be downloaded using the following command lines:

```bash
EUTILS="https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&rettype=fasta&id=";
t="Escherichia.coli";       s="K-12.MG1655";  a="U00096";   wget -q -O $t.$s.fasta $EUTILS$a ;
t="Klebsiella.pneumoniae";  s="NTUH-K2044";   a="AP006725"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Leptospira.interrogans"; s="FDAARGOS_203"; a="CP020414"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Listeria.monocytogenes"; s="EGD-e";        a="AL591824"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Mycoplasma.pneumoniae";  s="NCTC10119";    a="LR214945"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Neisseria.meningitidis"; s="11-7";         a="CP021520"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Salmonella.enterica";    s="LT2";          a="AE006468"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Shigella.flexneri";      s="2a.301";       a="AE005674"; wget -q -O $t.$s.fasta $EUTILS$a ;
t="Yersinia.pestis";        s="CO92";         a="AL590842"; wget -q -O $t.$s.fasta $EUTILS$a ;
```

A _RepeatPlot_ figure can be quickly built for each chromosome using the following example command lines:

```bash
RepeatPlot -i Escherichia.coli.K-12.MG1655.fasta        -o Escherichia.coli.K-12.MG1655.svg        -c black
RepeatPlot -i Klebsiella.pneumoniae.NTUH-K2044.fasta    -o Klebsiella.pneumoniae.NTUH-K2044.svg    -c blue
RepeatPlot -i Leptospira.interrogans.FDAARGOS_203.fasta -o Leptospira.interrogans.FDAARGOS_203.svg -c brown
RepeatPlot -i Listeria.monocytogenes.EGD-e.fasta        -o Listeria.monocytogenes.EGD-e.svg        -c gray
RepeatPlot -i Mycoplasma.pneumoniae.NCTC10119.fasta     -o Mycoplasma.pneumoniae.NCTC10119.svg     -c green
RepeatPlot -i Neisseria.meningitidis.11-7.fasta         -o Neisseria.meningitidis.11-7.svg         -c orange
RepeatPlot -i Salmonella.enterica.LT2.fasta             -o Salmonella.enterica.LT2.svg
RepeatPlot -i Shigella.flexneri.2a.301.fasta            -o Shigella.flexneri.2a.301.svg            -c sand
RepeatPlot -i Yersinia.pestis.CO92.fasta                -o Yersinia.pestis.CO92.svg                -c violet
```

This leads to the below figures:

<table border="0" cellspacing="0" cellpadding="0" style="text-align: center;" >
 <tr>
  <td width="33%"><br><b><em>E. coli</em> K-12 MG1655</b><img  align="center" width="100%" src="examples/Escherichia.coli.K-12.MG1655.svg"></td>
  <td width="33%"><br><b><em>K. pneumoniae</em> NTUH-K2044</b><img  align="center" width="100%" src="examples/Klebsiella.pneumoniae.NTUH-K2044.svg"></td>
  <td width="33%"><br><b><em>L. interrogans</em> FDAARGOS_203</b><img  align="center" width="100%" src="examples/Leptospira.interrogans.FDAARGOS_203.svg"></td>
 </tr>
 <tr>
 </tr>
 <tr>
  <td width="33%"><br><b><em>L. monocytogenes</em> EGD-e</b><img  align="center" width="100%" src="examples/Listeria.monocytogenes.EGD-e.svg"></td>
  <td width="33%"><br><b><em>M. pneumoniae</em> NCTC10119</b><img  align="center" width="100%" src="examples/Mycoplasma.pneumoniae.NCTC10119.svg"></td>
  <td width="33%"><br><b><em>N. meningitidis</em> 11-7</b><img  align="center" width="100%" src="examples/Neisseria.meningitidis.11-7.svg"></td>
 </tr>
 <tr>
 </tr>
 <tr>
  <td width="33%"><br><b><em>S. enterica</em> LT2</b><img  align="center" width="100%" src="examples/Salmonella.enterica.LT2.svg"></td>
  <td width="33%"><br><b><em>S. flexneri</em> 2a 301</b><img  align="center" width="100%" src="examples/Shigella.flexneri.2a.301.svg"></td>
  <td width="33%"><br><b><em>Y. pestis</em> CO92</b><img  align="center" width="100%" src="examples/Yersinia.pestis.CO92.svg"></td>
 </tr>
</table>




## References

 
Melsted P, Halldórsson PV (2014) _KmerStream: streaming algorithms for k-mer abundance estimation_. **Bioinformatics**, 30(24):3541-3547. [doi:10.1093/bioinformatics/btu71](https://doi.org/10.1093/bioinformatics/btu713).


Rocha EPC, Danchin A, Viari A (1999) _Analysis of Long Repeats in Bacterial Genomes Reveals Alternative Evolutionary Mechanisms in Bacillus subtilis and Other Competent Prokaryotes_. **Molecular Biology and Evolution**, 16(9):1219-1230. [doi:10.1093/oxfordjournals.molbev.a026212](https://doi.org/10.1093/oxfordjournals.molbev.a026212).


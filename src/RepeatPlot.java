/*
  ########################################################################################################

  RepeatPlot: creating figures that represent the positions of long repeats in a chromosome
    
  Copyright (C) 2022  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   Institut Pasteur, Paris, FRANCE           research.pasteur.fr/team/bioinformatics-and-biostatistics-hub

  ########################################################################################################
*/

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class RepeatPlot {

    //### constants  ################################################################
    final static String VERSION  = "1.2.220405ac        Copyright (C) 2022 Institut Pasteur";
    static final String NOTHING  = "N.o./.T.h.I.n.G";
    static final    int BUFFER   = 1<<16;
    static final   byte B0       = (byte) 0;
    static final   byte UTF8_A   = (byte) 65;
    static final   byte UTF8_C   = (byte) 67;
    static final   byte UTF8_G   = (byte) 71;
    static final   byte UTF8_N   = (byte) 78;
    static final   byte UTF8_T   = (byte) 84;
    static final   byte K        = 15;                      //## NOTE: k-mer length
    static final    int F0       = 1 << (2 * K);            //## NOTE: == 4^k
    static final   byte K_1      = K - 1;
    static final   byte BSIZE    = (byte) 2;                //## NOTE: no. bits per nucleotides
    static final   byte BA       = (byte) 0;                //## NOTE: A = 00
    static final   byte BC       = (byte) 1;                //## NOTE: C = 01
    static final   byte BG       = (byte) 2;                //## NOTE: G = 10
    static final   byte BT       = (byte) 3;                //## NOTE: T = 11
    static final    int CA       = BT << (BSIZE * K_1);     //## NOTE: °°°1100000000000000
    static final    int CC       = BG << (BSIZE * K_1);     //## NOTE: °°°1000000000000000
    static final    int CG       = BC << (BSIZE * K_1);     //## NOTE: °°°0100000000000000
    static final    int CT       = BA;                      //## NOTE: == 0, because BA == 0
    static final    int MSK      = (1 << (BSIZE * K)) - 1;  //## NOTE: °°°1111111111111111 == 2*k 1s
    static final double CUTOFF   = 0.9;
    static final    int SVGSIZE  = 5000;                    //## NOTE: final SVG file dimension
    static final  int[] AL       = {50000, 10000, 5000, 1000, 500, 100, 50, 25};
    static final String BLACK    = "#000000";
    static final String BLUE     = "#0067a5";
    static final String BROWN    = "#882d17";
    static final String GRAY     = "#848482";
    static final String GREEN    = "#008856";
    static final String ORANGE   = "#f38400";
    static final String RED      = "#be0032";
    static final String SAND     = "#c2b280";
    static final String VIOLET   = "#875692";
    
    //### options   #################################################################
    static File infile;    // -i
    static File outfile;   // -o
    static int minlgt;     // -l
    static int minsucc;    //## NOTE: = minRepeatLength - K
    static String color;   // -c
    static double rfactor; // -r
    static boolean log;    // -v

    //### io   ######################################################################
    static String filename;
    static BufferedReader in;
    static BufferedWriter out, oul;

    //### data   ####################################################################
    static byte[] seb;              //## NOTE: byte array repreentation of the input sequence
    static int lgt;                 //## NOTE: length of the input sequence
    static int nk;                  //## NOTE: no. non-unique canonical k-mers
    static int[] ahsh;              //## NOTE: hash value of each non-unique canonical k-mers
    static int nr;                  //## NOTE: no. regions composed by successive non-unique canonical k-mers
    static ArrayList<Integer> pos;  //## NOTE: starting position of each of the nr selected regions
    static ArrayList<BitSet> albs;  //## NOTE: hash values of the non-unique canonical k-mers constituting each of the nr selected regions
    static int np;                  //## NOTE: no. pairs of similar regions
    static int uf0, lf1;            //## NOTE: F0, f1
    static long uf1;                //## NOTE: F1
    
    //### drawing parameters   ######################################################
    static double gratio;
    static int side, border, x0, y0, xmax, ymax, step, lstroke, fontsize, mradius;
    
    //### stuffs   ##################################################################
    static byte skip;
    static int c, o, i, j, l, kmr, krc, hsh, succ, ci, s;
    static String line, fwd, rev;
    static StringBuilder sb;
    static BitSet bs, bs2;
    
    public static void main(String[] args) throws IOException {
	
	//#############################################################################################################
	//#############################################################################################################
	//### doc                                                                                                   ###
	//#############################################################################################################
	//#############################################################################################################
	System.out.println("");
	System.out.println("RepeatPlot v" + VERSION);
	if ( args.length < 4 ) {
	    System.out.println("");
	    System.out.println("USAGE: RepeatPlot -i <fasta> -o <svg> [-l <length>] [-c <color>] [-r <scale>] [-v]");
	    System.out.println("");
	    System.out.println("OPTIONS:");
	    System.out.println("  -i <file>    input FASTA file name; should end with .gz when gzipped (mandatory)");
	    System.out.println("  -o <file>    output SVG file name (mandatory)");
	    System.out.println("  -l <int>     minimum repeat region length (default: 50)");
	    System.out.println("  -c <string>  circle color: black, blue,  brown, gray, green,  orange, red, sand,");
	    System.out.println("               violet (default: red)");
	    System.out.println("  -r <real>    radius scale factor (default: 1.0)");
	    System.out.println("  -v           prints info into log file (default: not set)");
	    System.out.println("");
	    /* System.out.println(" A  " + Integer.toBinaryString(BA));
	       System.out.println(" C  " + Integer.toBinaryString(BC));
	       System.out.println(" G  " + Integer.toBinaryString(BG));
	       System.out.println(" T  " + Integer.toBinaryString(BT));
	       System.out.println("rA  " + Integer.toBinaryString(CA));
	       System.out.println("rC  " + Integer.toBinaryString(CC));
	       System.out.println("rG  " + Integer.toBinaryString(CG));
	       System.out.println("rT  " + Integer.toBinaryString(CT));
	       System.out.println("msk " + Integer.toBinaryString(MSK));
	       System.out.println("del " + Integer.toBinaryString(DEL));
	       System.out.println("drc " + Integer.toBinaryString(DRC));
	       System.out.println("F0  " + F0);
	       System.out.println(""); */
	    System.exit(0);
	}
	
	//#############################################################################################################
	//#############################################################################################################
	//### reading options                                                                                       ###
	//#############################################################################################################
	//#############################################################################################################
	infile  = new File(NOTHING);  // -i
	outfile = new File(NOTHING);  // -o
	minlgt = 50;                  // -l
	color = "red";                // -c
	rfactor = 1.0;                // -r
	log = false;                  // -v
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     {  infile = new File(args[++o]);           continue; }
	    if ( args[o].equals("-o") )     { outfile = new File(args[++o]);           continue; }
	    if ( args[o].equals("-l") ) try {  minlgt = Integer.parseInt(args[++o]);   continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -l)"); System.exit(1); }
	    if ( args[o].equals("-c") )     {   color = args[++o];                     continue; }
	    if ( args[o].equals("-r") ) try { rfactor = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -r)"); System.exit(1); }
	    if ( args[o].equals("-v") )     {     log = true;                          continue; }
	}
	filename = infile.toString();
	if ( filename.equals(NOTHING) ) { System.err.println("no input file (option -i)");                      System.exit(1); }
	if ( ! infile.exists() )        { System.err.println("infile does not exist (option -i): " + filename); System.exit(1); }
	filename = outfile.toString();
	if ( filename.equals(NOTHING) ) { System.err.println("no output file (option -o)");                     System.exit(1); }
	switch ( color.toUpperCase() ) {
	case "BLUE":   color = BLUE;   break;
	case "BROWN":  color = BROWN;  break;
	case "GRAY":   color = GRAY;   break;
	case "GREEN":  color = GREEN;  break;
	case "ORANGE": color = ORANGE; break;
	case "RED":    color = RED;    break;
	case "SAND":   color = SAND;   break;
	case "VIOLET": color = VIOLET; break;
	default:       color = BLACK;  break;
	}
	if ( rfactor <= 0 )             { System.err.println("should be positive (option -r): " + rfactor);     System.exit(1); }
	if ( minlgt < 25 )              { System.err.println("should be at least 25 (option -l): " + minlgt);   System.exit(1); }
	minsucc = minlgt - K;
		
	System.out.println("k-mer length                    " + K);
	System.out.println("min. repeat region length       " + minlgt);

	
	//#############################################################################################################
	//#############################################################################################################
	//### reading infile                                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	filename = infile.toString();

	System.out.println("input file                      " + filename);

	in = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	sb = new StringBuilder("");
	c = 0;
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.startsWith(">") ) {
		++c;
		sb = sb.append("X");
		continue;
	    }
	    sb = sb.append(line.toUpperCase());
	}
	seb = sb.substring(1).replaceAll(" ", "").getBytes(StandardCharsets.UTF_8);
	--c;
	lgt = seb.length;

	System.out.println("total sequence length           " + (lgt-c));
	

	//#############################################################################################################
	//#############################################################################################################
	//### traversing k-mers to find non-unique ones                                                             ###
	//#############################################################################################################
	//#############################################################################################################
	bs = new BitSet(F0);   //## NOTE: the hash of every traversed k-mers
	bs2 = new BitSet(F0);  //## NOTE: the hash of every k-mer traversed twice
	kmr = krc = skip = B0;
	i = -1;
	while ( ++i < K_1 ) {
	    kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > B0 ) ? --skip : skip;
	    switch ( seb[i] ) { 
	    case UTF8_A: /*kmr |= BA;*/ krc |= CA;   continue;
	    case UTF8_C:   kmr |= BC;   krc |= CC;   continue;
	    case UTF8_G:   kmr |= BG;   krc |= CG;   continue;
	    case UTF8_T:   kmr |= BT; /*krc |= CT;*/ continue;
	    default:                    skip = K;    continue; 
	    }
	}
	uf1 = 0;
	--i;
	while ( ++i < lgt ) {
	    kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
	    switch ( seb[i] ) { 
	    case UTF8_A: /*kmr |= BA;*/ krc |= CA;   break;
	    case UTF8_C:   kmr |= BC;   krc |= CC;   break;
	    case UTF8_G:   kmr |= BG;   krc |= CG;   break;
	    case UTF8_T:   kmr |= BT; /*krc |= CT;*/ break;
	    default:                    skip = K;    continue; 
	    }
	    if ( skip > B0 ) continue;
	    hsh = ( (kmr&=MSK) < krc ) ? kmr : krc;
	    if ( bs.get(hsh) ) bs2.set(hsh);
	    else bs.set(hsh);
	    ++uf1;
	}

	uf0 = bs.cardinality();
	lf1 = uf0 - bs2.cardinality();

	System.out.println("total no. k-mers (F1)           " + uf1);
	System.out.println("no. distinct k-mers (F0)        " + uf0);
	System.out.println("no. unique k-mers (f1)          " + lf1);

		
	//#############################################################################################################
	//#############################################################################################################
	//### storing the hash of each non-unique k-mer                                                             ###
	//#############################################################################################################
	//#############################################################################################################
	nk = bs2.cardinality();
	ahsh = new int[nk];
	i = hsh = -1;
	while ( (hsh=bs2.nextSetBit(++hsh)) >= 0 ) ahsh[++i] = hsh;

	System.out.println("no. non-unique k-mers (F0-f1)   " + nk);
	System.out.println("k-mer repeat index (F1-f1)/F0   " + String.format(Locale.US, "%.5f", ((uf1-lf1)/((double)uf0))));;

	if ( log ) oul = new BufferedWriter(new FileWriter(new File(outfile.toString() + ".log")));

	
	//#############################################################################################################
	//#############################################################################################################
	//### looking for putative repeat regions                                                                   ###
	//#############################################################################################################
	//#############################################################################################################
	pos = new ArrayList<Integer>();    //## NOTE: position of each putative repeat region
	albs = new ArrayList<BitSet>();    //## NOTE: k-mer content of each putative repeat region
	kmr = krc = skip = B0;
	i = -1;
	while ( ++i < K_1 ) {
	    kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > B0 ) ? --skip : skip;
	    switch ( seb[i] ) { 
	    case UTF8_A: /*kmr |= BA;*/ krc |= CA;   continue;
	    case UTF8_C:   kmr |= BC;   krc |= CC;   continue;
	    case UTF8_G:   kmr |= BG;   krc |= CG;   continue;
	    case UTF8_T:   kmr |= BT; /*krc |= CT;*/ continue;
	    default:                    skip = K;    continue; 
	    }
	}
	succ = 0;
	bs = new BitSet(nk);
	--i;
	while ( ++i < lgt ) {
	    kmr <<= BSIZE; krc >>>= BSIZE; skip = ( skip > 0 ) ? --skip : skip;
	    switch ( seb[i] ) { 
	    case UTF8_A: /*kmr |= BA;*/ krc |= CA;   break;
	    case UTF8_C:   kmr |= BC;   krc |= CC;   break;
	    case UTF8_G:   kmr |= BG;   krc |= CG;   break;
	    case UTF8_T:   kmr |= BT; /*krc |= CT;*/ break;
	    default:                    skip = K;    continue; 
	    }
	    if ( skip > B0 ) continue;
	    hsh = ( (kmr&=MSK) < krc ) ? kmr : krc;
	    if ( bs2.get(hsh) ) {
		++succ;
		bs.set(Arrays.binarySearch(ahsh, hsh));
		continue;
	    }
	    if ( succ > minsucc ) {
		pos.add(i-succ);
		albs.add(bs.get(0, nk));
		if ( log ) {
		    oul.write("R" + pos.size() + "\t" + (new String(Arrays.copyOfRange(seb, i-succ-K_1, i), StandardCharsets.UTF_8)));
		    oul.newLine();
		}
	    }
	    succ = 0;
	    bs.clear();
	}
	nr = pos.size();

	System.out.println("no. repeat regions              " + nr);
	

	//#############################################################################################################
	//#############################################################################################################
	//### drawing SVG figure                                                                                    ###
	//#############################################################################################################
	//#############################################################################################################
	border = 5 * lgt / 100;                      //## NOTE: border size
	side = lgt + 2 * border;                     //## NOTE: figure size
	gratio = SVGSIZE / (double) side;            //## NOTE: global ratio to obtain a final SVGSIZExSVGSIZE figure file
	lstroke = (int) (1 + 10 / gratio);           //## NOTE: line stroke
	fontsize = lgt / 70;                         //## NOTE: font size
	mradius = 1 + (int) (rfactor * lgt / 1500);  //## NOTE: minimum circle radius

	x0 = border;
	y0 = border + lgt;
	xmax = x0 + lgt;
	ymax = border;
		
	out = new BufferedWriter(new FileWriter(outfile));

	out.write("<svg version=\"1.1\" width=\"" + SVGSIZE + "\" height=\"" + SVGSIZE + "\" xmlns=\"http://www.w3.org/2000/svg\">");
	out.newLine(); out.newLine();

	out.write(" <style>");
	out.newLine();
	out.write("  text {");
	out.newLine();
	out.write("   font-family: Arial, Helvetica, sans-serif;");
	out.newLine();
	out.write("   fill: black;");
	out.newLine();
	out.write(" }");
	out.newLine();
	out.write(" </style>");
	out.newLine(); out.newLine();

	out.write(" <!-- background -->");
	out.newLine(); out.newLine();
	out.write(" <rect width=\"100%\" height=\"100%\" fill=\"white\"/>");
	out.newLine(); out.newLine();

	out.write(" <g transform=\"scale(" + String.format(Locale.US, "%.9f", gratio) + ")\">");
	out.newLine(); out.newLine();

	out.write("  <!-- frame -->");
	out.newLine(); out.newLine();
	out.write(String.format(Locale.US, "  <polyline points=\"%d,%d %d,%d %d,%d %d,%d %d,%d\"", x0, y0, xmax, y0, xmax, ymax, x0, ymax, x0, y0));
	out.write(" fill=\"none\" stroke=\"#848482\" stroke-linecap=\"round\" stroke-width=\"" + lstroke + "\"/>");
	out.newLine(); out.newLine();
	step = lgt / 10;
	step = ( lgt > 10000 )   ?    100 * (step / 100)
	    :  ( lgt > 100000 )  ?   1000 * (step / 1000)
	    :  ( lgt > 1000000 ) ?  10000 * (step / 10000)
	    :                      100000 * (step / 100000);
	l = fontsize / 2; 
	i = -step;
	while ( (i+=step) < lgt ) {
	    out.write(String.format(Locale.US, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\"", x0+i, y0, x0+i, y0+l));
	    out.write(" stroke=\"#848482\" stroke-linecap=\"round\" stroke-width=\"" + lstroke + "\"/>");
	    out.newLine(); out.newLine();
	    out.write(String.format(Locale.US, "  <text x=\"%d\" y=\"%d\" font-size=\"%d\" text-anchor=\"middle\">%d</text>", x0+i, y0+2*fontsize, fontsize, i));
	    out.newLine(); out.newLine();
	    out.write(String.format(Locale.US, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\"", x0-l, y0-i, x0, y0-i));
	    out.write(" stroke=\"#848482\" stroke-linecap=\"round\" stroke-width=\"" + lstroke + "\"/>");
	    out.newLine(); out.newLine();
	    out.write(String.format(Locale.US, "  <text x=\"%d\" y=\"%d\" transform=\"rotate(-90,%d,%d)\" font-size=\"%d\" text-anchor=\"middle\">%d</text>", x0-l-fontsize, y0-i, x0-l-fontsize, y0-i, fontsize, i));
	    out.newLine(); out.newLine();
	    out.write(String.format(Locale.US, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\"", x0+i, ymax, x0+i, ymax-l));
	    out.write(" stroke=\"#848482\" stroke-linecap=\"round\" stroke-width=\"" + lstroke + "\"/>");
	    out.newLine(); out.newLine();
	    out.write(String.format(Locale.US, "  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\"", xmax, y0-i, xmax+l, y0-i));
	    out.write(" stroke=\"#848482\" stroke-linecap=\"round\" stroke-width=\"" + lstroke + "\"/>");
	    out.newLine(); out.newLine();
	}

	out.write("  <!-- legend -->");
	out.newLine(); out.newLine();
	step /= 3;
	i = 0;
	for (int l: AL) {
	    ++i;
	    if ( l >= minlgt ) {
		out.write(String.format(Locale.US, "  <circle cx=\"%d\" cy=\"%d\" r=\"%d\" fill=\"%s\" fill-opacity=\"0.6\"/>", xmax-step, y0-i*step, radius(l, mradius), color));
		out.newLine(); out.newLine();
		out.write(String.format(Locale.US, "  <text x=\"%d\" y=\"%d\" font-size=\"%d\" text-anchor=\"end\">%d</text>", xmax-2*step, y0-i*step+fontsize/3, fontsize, l));
		out.newLine(); out.newLine();
	    }
	}

	out.write("  <!-- circles -->");
	out.newLine(); out.newLine();
	np = 0;
	i = -1;
	while ( ++i < nr ) {
	    bs = albs.get(i).get(0, nk);
	    ci = border + lgt-pos.get(i) + bs.cardinality()/2;
	    j = -1;
	    while ( ++j < i ) {
		if ( (s=cardint(bs, albs.get(j))) > minsucc ) {
		    out.write(String.format(Locale.US, "  <circle cx=\"%d\" cy=\"%d\" r=\"%d\" fill=\"%s\" fill-opacity=\"0.6\"/>", border+pos.get(j)+albs.get(j).cardinality()/2, ci, radius(s+K_1, mradius), color));
		    out.newLine(); out.newLine();
		    ++np;
		    if ( log ) {
			oul.write("R" + (i+1) + "\tR" + (j+1) + "\t" + s);
			oul.newLine();
		    }
		}
	    }
	}

	out.write(" </g>");
	out.newLine(); out.newLine();
	out.write("</svg>");
	out.newLine(); out.newLine();

	System.out.println("no. pairs of similar regions    " + np);

	//System.out.println("repeat index                 " + String.format(Locale.US, "%.9f", (index_up/(index_dn*index_dn))));

	filename = outfile.toString();
	if ( log ) {
	    System.out.println("log file                        " + filename + ".log");
	    oul.close();
	}
	out.close();
	System.out.println("output file                     " + filename);
	System.out.println("");
    }

    // returns the radius of a circle from its associated length given a minimum radius
    private static int radius (final double rlgt, final int minRadius) {
	return (int) (minRadius * (1 + Math.log(rlgt/25) / Math.log(1.8)));
    }

    // returns the cardinality of the intersection of the two specified Bitset 
    private static int cardint (final BitSet b1, final BitSet b2) {
	if ( ! b1.intersects(b2) ) return 0;
	BitSet b = b1.get(0, b1.length());
	b.and(b2);
	return b.cardinality();
    }
    
}

